# DeepLearning with Tensorflow

Hi! in this repository you will find my work related to  **DeepLearning** mostly using **TensorFlow** toolkit. 

## src/YOLO_small

YOLO_small folder contains my implementation of a neural network presented in [this](https://arxiv.org/pdf/1506.02640.pdf) paper. For sake of simplicity I have not implemented training part YOLO instead I am using a pre-trained network weights available [here](https://drive.google.com/file/d/0B2JbaJSrWLpza08yS2FSUnV2dlE/view?usp=sharing).  To run this script download the `YOLO_small.ckpt` file from the given link and place it in folder `src/YOLO_small/weights`and then run the following command on your terminal window
> `python my_yolo_small_tf.py -fromfile "test/person.jpg" -tofile_img "result.jpg"`


